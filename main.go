package main

import (
	"encoding/json"
	"fmt"
	m "github.com/abhsk/hello-go-world/models"
	"github.com/gorilla/mux"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strings"
)

func main() {
	// if len(os.Args) < 2 {
	// 	fmt.Println("Usage: main <port>")
	// 	return
	// }

	port := os.Getenv("PORT")
	port = ":" + port

	r := mux.NewRouter()
	r.HandleFunc("/", HellowHandler()).Methods("GET")
	r.HandleFunc("/trial123/", HelloPostHandler()).Methods("Post")
	r.HandleFunc("/health", HellowHandler()).Methods("GET")

	// Bind to a port and pass our router in
	log.Fatal(http.ListenAndServe(port, r))
}

func HellowHandler() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		raw, err := ioutil.ReadFile("./data/data.json")
		if err != nil {
			fmt.Println(err)
		}
		fmt.Fprintf(w, string(raw))
	}
}

func HelloPostHandler() http.HandlerFunc {
	return func(w http.ResponseWriter, req *http.Request) {
		decoder := json.NewDecoder(req.Body)
		var chatMessage *m.ChatMessage
		err := decoder.Decode(&chatMessage)
		if err != nil {
			panic(err)
		}
		defer req.Body.Close()

		log.Printf(chatMessage.Type)

		message1 := strings.SplitAfterN(chatMessage.Message.Text, "@Trial ", 2)
		realMessage := message1[0]
		if len(message1) > 1 {
			realMessage = message1[1]
		}

		parsedMessage := strings.Split(realMessage, " ")

		action := "Default Value"
		pair1 := "Default Value"
		pair2 := "Default Value"

		if len(parsedMessage) == 3 {
			action = parsedMessage[0]
			pair1 = parsedMessage[1]
			pair2 = parsedMessage[2]

		}

		w.Header().Set("Content-Type", "application/json")
		w.Write([]byte(`{ "text": "Did you say: ` + action + ":" + pair1 + ":" + pair2 + `"}`))
	}
}
